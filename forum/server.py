from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from controller.index import *

def main():
    config = Configurator()
    config.include('pyramid_chameleon')
    config.add_route('index', '/')
    config.add_route('topic', '/topic')
    config.scan("controller")
    app = config.make_wsgi_app()
    return app

if __name__ == '__main__':
    app = main()
    server = make_server('0.0.0.0', 6547, app)
    print ('Starting up server on http://localhost:6547')
    server.serve_forever()
