# -*- coding: utf-8 -*- 
from pyramid.response import Response
from model.model import *
from pyramid.view import view_config


@view_config(route_name='index', renderer='index.pt')
def index(request):
    if request:
        attributes = request.GET
	
	try:
            new_topic = attributes["new_topic"]
	    new_topic_desc = attributes["new_topic_desc"]
            new_topic_owner = attributes["new_topic_owner"]
	    if new_topic_desc == "":
 	        new_topic_desc = "No description"
	    insert_topic(new_topic, new_topic_desc, new_topic_owner)
	except:
	    pass

    result = select_topic_by_datetime()
    
    return {"result_list": result}
